#
# Copyright (C) 2009-2012 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#
include $(TOPDIR)/rules.mk

ARCH:=arm
BOARD:=foxcomm
BOARDNAME:=FoxComm Gpon Brd
FEATURES:=targz usb jffs2
MAINTAINER:=foxComm <info@foxmomm.ru>

define Target/Description
	FoxComm board on Marvell 88F6283 
endef

LINUX_VERSION:=3.3.8

include $(INCLUDE_DIR)/target.mk

KERNELNAME:="uImage"

DEFAULT_PACKAGES +=

$(eval $(call BuildTarget))
