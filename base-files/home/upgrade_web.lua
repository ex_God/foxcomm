#!/usr/bin/lua

local lsleep    = require 'lsleep'

function execute_shell_cmd (shell_cmd,delay)
    local file = io.popen(shell_cmd)
    local output = file:read("*a")
    file:close()
    if ( delay ) then
	lsleep.msleep(delay)
    end
    return ( output )
end 

function find_and_sub_string (init_str,pattern_str,sub_len)
    local _,i = string.find(init_str,pattern_str)

    if (i ~= nil) then
	return ( string.sub(init_str, (i+1), (sub_len==nil) and (#init_str-1) or (i+sub_len)) )
    else
	return nil
    end
end

local error_flag=0,i,f

-- определяем активный раздел
local active_ubi = tonumber ( find_and_sub_string(execute_shell_cmd("cat /proc/cmdline"),"ubi.mtd=", 1) )
local active_part = ((active_ubi == 5) and "1" or "2")
print ("Determine active partition.......\t" .. active_part)

-- считываем hw, kernel, fw версии 
local hw_ver = find_and_sub_string(execute_shell_cmd(string.format ("printEnv_%d hw_ver",active_part)),"hw_ver=")
print (string.format ("Hardware version \t\t\t%s",hw_ver))

execute_shell_cmd (string.format ("cat /dev/mtd%d > /tmp/mtd",(active_ubi-2)))
local kernel_ver = find_and_sub_string(execute_shell_cmd("/home/kern_ver -l /tmp/mtd 2>/dev/null"),"Image Name:   ")
print (string.format ("Current kernel version \t\t\t%s",kernel_ver))

local ubi_ver = find_and_sub_string(execute_shell_cmd("/home/ubi_name /dev/ubi0_0"),"Name:        ")
print (string.format ("Current ubifs version \t\t\t%s",ubi_ver))

if (active_ubi == 5 or active_ubi == 6) then
    local result,boot_priority_1,boot_priority_2
--[[   
    -- получим и распакуем архив с новым ПО
    io.write("Get new firmware archive.......\t\t")
    execute_shell_cmd ("mkdir -p /tmp/firmware/")
    execute_shell_cmd ("tftp -g -r foxComm/mrv/firmware.tar.gz 192.168.1.10 -l /tmp/firmware/firmware.tar.gz")
    io.write("Ok\n")
]]--    
    io.write("Untar archive .......\t\t\t")
    execute_shell_cmd ("tar -xvzf /tmp/firmware/firmware.tar.gz -C /tmp/firmware")
    io.write("Ok\n")
    
    -- считаем fw и kernel версии из полученного архива
    kernel_ver = execute_shell_cmd("/home/kern_ver -l /tmp/firmware/images/uImage 2>/dev/null")
    if ( find_and_sub_string(kernel_ver,hw_ver) ) then
	kernel_ver = find_and_sub_string(kernel_ver,"Image Name:   ")
	print (string.format ("New kernel version \t\t\t%s",kernel_ver))
    else
    	print (string.format ("Error! Wrong kernel image"))
	error_flag = 1;
    end

    execute_shell_cmd("dmesg -n 1")
    execute_shell_cmd("depmod")
    execute_shell_cmd("modprobe nandsim first_id_byte=0xec second_id_byte=0xdc third_id_byte=0x00 fourth_id_byte=0x15")
    execute_shell_cmd("flash_erase /dev/mtd7 0 0 > /dev/null 2>/dev/null")
    execute_shell_cmd("nandwrite -p /dev/mtd7 /tmp/firmware/images/ubi.img > /dev/null 2>/dev/null")
    execute_shell_cmd("ubidetach /dev/ubi_ctrl -m 7 > /dev/null 2>/dev/null")
    execute_shell_cmd("ubiattach /dev/ubi_ctrl -m 7 > /dev/null 2>/dev/null")
   
    ubi_ver = execute_shell_cmd ("/home/ubi_name /dev/ubi1_0")
    if (find_and_sub_string(ubi_ver,hw_ver)) then
        ubi_ver = find_and_sub_string(ubi_ver, "Name:        ")
	print (string.format ("New ubifs version \t\t\t%s",ubi_ver))    	    
    else
	print (string.format ("Error! Wrong ubi image"))
	error_flag = 1;        
    end
    
    execute_shell_cmd("ubidetach /dev/ubi_ctrl -m 7 > /dev/null 2>/dev/null")
    execute_shell_cmd("rmmod nandsim > /dev/null 2>/dev/null")

    if (error_flag == 0) then
	if (active_ubi == 5) then
	    -- очистим разделы с env, kernel, ubifs
	    io.write("Erase .......\t\t\t\t")
	    execute_shell_cmd("flash_erase /dev/mtd2 0 0")
	    execute_shell_cmd("flash_erase /dev/mtd4 0 0")
	    execute_shell_cmd("flash_erase /dev/mtd6 0 0")
	    io.write("Ok\n")	

	    -- перепишем user cfg
	    io.write("Copy user config .......\t\t")
	    execute_shell_cmd("cat /dev/mtd1 > /tmp/mtd")
	    execute_shell_cmd("nandwrite -p /dev/mtd2 /tmp/mtd > /dev/null 2>/dev/null")
	    execute_shell_cmd("setEnv_2 _erase-kernel 'nand erase clean kernel2'")
	    execute_shell_cmd("setEnv_2 _erase-rootfs 'nand erase clean ubifs2'")
	    execute_shell_cmd("setEnv_2 bootcmd 'nand read.e $(loadaddr) kernel2; bootm $(loadaddr)'")
	    execute_shell_cmd("setEnv_2 bootargs 'console=ttyS0,115200 ubi.mtd=6 root=ubi0 rootfstype=ubifs'")
	    io.write("Ok\n")	
	
	    -- запишем новые kernel и ubifs
	    io.write("Write kernel and ubifs .......\t\t")
	    execute_shell_cmd("nandwrite -p /dev/mtd4 /tmp/firmware/images/uImage > /dev/null 2>/dev/null")
	    execute_shell_cmd("nandwrite -p /dev/mtd6 /tmp/firmware/images/ubi.img > /dev/null 2>/dev/null")
	    io.write("Ok\n")

	    -- изменим активный раздел
	    io.write("Change active partition .......\t\t")
	
	    boot_priority_1 = tonumber (find_and_sub_string(execute_shell_cmd("printEnv_1 boot_priority"),"boot_priority="))
	    boot_priority_2 = tonumber (find_and_sub_string(execute_shell_cmd("printEnv_2 boot_priority"),"boot_priority="))

	    boot_priority_2 = boot_priority_1 + 1
	    execute_shell_cmd("setEnv_2 boot_priority " .. boot_priority_2)	

	    -- выставим флаг что произошло обновление
	    execute_shell_cmd("setEnv_2 new_fw_cnt new")		
	
	    io.write("Ok\n")
	
	elseif (active_ubi == 6) then
	    -- очистим разделы с env, kernel, ubifs
	    io.write("Erase .......\t\t\t\t")
	    execute_shell_cmd("flash_erase /dev/mtd1 0 0")
	    execute_shell_cmd("flash_erase /dev/mtd3 0 0")
	    execute_shell_cmd("flash_erase /dev/mtd5 0 0")
	    io.write("Ok\n")
	    
	    -- перепишем user cfg
	    io.write("Copy user config .......\t\t")
	    execute_shell_cmd("cat /dev/mtd2 > /tmp/mtd")
	    execute_shell_cmd("nandwrite -p /dev/mtd1 /tmp/mtd > /dev/null 2>/dev/null")
	    execute_shell_cmd("setEnv_1 _erase-kernel 'nand erase clean kernel1'")
	    execute_shell_cmd("setEnv_1 _erase-rootfs 'nand erase clean ubifs1'")
	    execute_shell_cmd("setEnv_1 bootcmd 'nand read.e $(loadaddr) kernel1; bootm $(loadaddr)'")
	    execute_shell_cmd("setEnv_1 bootargs 'console=ttyS0,115200 ubi.mtd=5 root=ubi0 rootfstype=ubifs'")
	    io.write("Ok\n")
		
	    -- запишем новые kernel и ubifs
	    io.write("Write kernel and ubifs .......\t\t")
	    execute_shell_cmd("nandwrite -p /dev/mtd3 /tmp/firmware/images/uImage > /dev/null 2>/dev/null")
	    execute_shell_cmd("nandwrite -p /dev/mtd5 /tmp/firmware/images/ubi.img > /dev/null 2>/dev/null")
	    io.write("Ok\n")

	    -- изменим активный раздел
	    io.write("Change active partition .......\t\t")
	
	    boot_priority_1 = tonumber (find_and_sub_string(execute_shell_cmd("printEnv_1 boot_priority"),"boot_priority="))
	    boot_priority_2 = tonumber (find_and_sub_string(execute_shell_cmd("printEnv_2 boot_priority"),"boot_priority="))	    

	    boot_priority_1 = boot_priority_2 + 1
	    execute_shell_cmd("setEnv_1 boot_priority " .. boot_priority_1)
	    
	    -- выставим флаг что произошло обновление
	    execute_shell_cmd("setEnv_1 new_fw_cnt new")
	
	    io.write("Ok\n")
	    
	end
--[[
    print ("Upgrade successfully! Reboot device.")
    execute_shell_cmd("reboot")
]]--	
    else
	print ("Stop upgrade")
    end

end



