#!/usr/bin/lua

local f = io.popen("cat /proc/cmdline")
local cmdline = f:read("*a")
f:close()

local i = string.find(cmdline,"ubi.mtd=")
local active_part = string.sub( cmdline, (i+8), (i+8) )
active_part = tonumber (active_part)

print ("Determine active partition....... " .. ((active_part == 5) and "1" or "2"))

local result_1,result_2,upgrade_status_1,upgrade_status_2,i,j

io.write("Check upgrade status ....... ")

if 	(active_part == 5) then
	
    f = io.popen("printEnv_1 new_fw_cnt 2>/dev/null")
    result_1 = f:read("*a")    
    _,i = string.find(result_1,"new_fw_cnt=")
    
--    f = io.popen("printEnv_2 new_fw_cnt 2>/dev/null")
--    result_2 = f:read("*a")    
--    _,j = string.find(result_2,"new_fw_cnt=") 
    
--    if ( j ~= nil ) then
--	upgrade_status_2 = string.sub( result_2, (j+1), (#result_2 - 1) )
--    end
    
    if ( i ~= nil ) then
        upgrade_status_1 = string.sub( result_1, (i+1), (#result_1 - 1) )
	io.write(string.format ("%s\n",upgrade_status_1 ))
	
--	if (upgrade_status_1 == "start" and upgrade_status_2 ~= "upgrade_done") then
	if (upgrade_status_1 == "start") then	
	    print ("Change upgrade status ....... Ok")
	    f = io.popen("setEnv_1 new_fw_cnt upgrade_done")		
	end
    else
	io.write(string.format ("%s\n","status not found" ))
    end
    
elseif (active_part == 6) then
    f = io.popen("printEnv_2 new_fw_cnt 2>/dev/null")
    result_1 = f:read("*a")    
    _,i = string.find(result_1,"new_fw_cnt=")
    
--    f = io.popen("printEnv_1 new_fw_cnt 2>/dev/null")
--    result_2 = f:read("*a")    
--    _,j = string.find(result_2,"new_fw_cnt=") 
    
--    if ( j ~= nil ) then
--	upgrade_status_2 = string.sub( result_2, (j+1), (#result_2 - 1) )
--    end
    
    if ( i ~= nil ) then
        upgrade_status_1 = string.sub( result_1, (i+1), (#result_1 - 1) )
	io.write(string.format ("%s\n",upgrade_status_1 ))
	
--	if (upgrade_status_1 == "start" and upgrade_status_2 ~= "upgrade_done") then
	if (upgrade_status_1 == "start" ) then
	    print ("Change upgrade status ....... Ok")
	    f = io.popen("setEnv_2 new_fw_cnt upgrade_done")		
	end
    else
	io.write(string.format ("%s\n","status not found" ))
    end
end


