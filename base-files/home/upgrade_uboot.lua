#!/usr/bin/lua

local lsleep    = require 'lsleep'

function execute_shell_cmd (shell_cmd,delay)
    local file = io.popen(shell_cmd)
    local output = file:read("*a")
    file:close()
    if ( delay ) then
	lsleep.msleep(delay)
    end
    return ( output )
end 

-- получим и распакуем архив с новым ПО
io.write("Get new u-boot image.......\t\t")
execute_shell_cmd ("mkdir -p /tmp/firmware/images")
execute_shell_cmd ("tftp -g -r foxComm/mrv/u-boot.bin 192.168.1.10 -l /tmp/firmware/images/u-boot.bin")
io.write("Ok\n")
    
io.write("Erase .......\t\t\t\t")
execute_shell_cmd("flash_erase /dev/mtd0 0 0")
io.write("Ok\n")	

-- запишем новые kernel и ubifs
io.write("Write new u-boot image .......\t\t")
execute_shell_cmd("nandwrite -p /dev/mtd0 /tmp/firmware/images/u-boot.bin > /dev/null 2>/dev/null")
io.write("Ok\n")
